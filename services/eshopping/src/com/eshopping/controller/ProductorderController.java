package com.eshopping.controller; 

// Generated 17 Jun, 2014 10:42:00 AM


import com.eshopping.service.ProductorderService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eshopping.*;

/**
 * Controller object for domain model class Productorder.
 * @see com.eshopping.Productorder
 */

@RestController
@RequestMapping("/eshopping/Productorder")
public class ProductorderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductorderController.class);

	@Autowired
	private ProductorderService service;

	/**
	 * Processes requests to return lists all available Productorders.
	 * 
	 * @param model
	 * @return The name of the  Productorder list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Productorder> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Productorders list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Productorder> getProductorders(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Productorders list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Productorders");
		Long count = service.countAll();
		return count;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Productorder getProductorder(@PathVariable("id") Integer id) throws EntityNotFoundException {
		LOGGER.debug("Getting person with id: {}" , id);
		Productorder instance = service.findById(id);
		LOGGER.debug("Productorder details with id: {}" , instance);
		return instance;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public boolean delete(@PathVariable("id") Integer id) throws EntityNotFoundException {
		LOGGER.debug("Deleting Productorder with id: {}" , id);
		Productorder deleted = service.delete(id);
		return deleted != null;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Productorder editProductorder(@PathVariable("id") Integer id, @RequestBody Productorder instance) throws EntityNotFoundException {
        LOGGER.debug("Editing Productorder with id: {}" , instance.getId());
        instance.setId(id);
		instance = service.update(instance);
		LOGGER.debug("Productorder details with id: {}" , instance);
		return instance;
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Productorder createProductorder(@RequestBody Productorder instance) {
		LOGGER.debug("Create Productorder with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Productorder with information: {}" , instance);
	    return instance;
	}
	
	@org.springframework.web.bind.annotation.ExceptionHandler(EntityNotFoundException.class)
	ResponseEntity<String> handleEntityNotFoundException(Exception e) {
		return new ResponseEntity<String>(String.format("{\"Error\":\"%s\"}",
				e.getMessage()), HttpStatus.NOT_FOUND);
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setProductorderService(ProductorderService service) {
		this.service = service;
	}
}

