package com.eshopping.controller; 

// Generated 17 Jun, 2014 10:42:00 AM


import com.eshopping.service.OrderdetailService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eshopping.*;

/**
 * Controller object for domain model class Orderdetail.
 * @see com.eshopping.Orderdetail
 */

@RestController
@RequestMapping("/eshopping/Orderdetail")
public class OrderdetailController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderdetailController.class);

	@Autowired
	private OrderdetailService service;

	/**
	 * Processes requests to return lists all available Orderdetails.
	 * 
	 * @param model
	 * @return The name of the  Orderdetail list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Orderdetail> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Orderdetails list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Orderdetail> getOrderdetails(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Orderdetails list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Orderdetails");
		Long count = service.countAll();
		return count;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Orderdetail getOrderdetail(@PathVariable("id") Integer id) throws EntityNotFoundException {
		LOGGER.debug("Getting person with id: {}" , id);
		Orderdetail instance = service.findById(id);
		LOGGER.debug("Orderdetail details with id: {}" , instance);
		return instance;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public boolean delete(@PathVariable("id") Integer id) throws EntityNotFoundException {
		LOGGER.debug("Deleting Orderdetail with id: {}" , id);
		Orderdetail deleted = service.delete(id);
		return deleted != null;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Orderdetail editOrderdetail(@PathVariable("id") Integer id, @RequestBody Orderdetail instance) throws EntityNotFoundException {
        LOGGER.debug("Editing Orderdetail with id: {}" , instance.getId());
        instance.setId(id);
		instance = service.update(instance);
		LOGGER.debug("Orderdetail details with id: {}" , instance);
		return instance;
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Orderdetail createOrderdetail(@RequestBody Orderdetail instance) {
		LOGGER.debug("Create Orderdetail with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Orderdetail with information: {}" , instance);
	    return instance;
	}
	
	@org.springframework.web.bind.annotation.ExceptionHandler(EntityNotFoundException.class)
	ResponseEntity<String> handleEntityNotFoundException(Exception e) {
		return new ResponseEntity<String>(String.format("{\"Error\":\"%s\"}",
				e.getMessage()), HttpStatus.NOT_FOUND);
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setOrderdetailService(OrderdetailService service) {
		this.service = service;
	}
}

