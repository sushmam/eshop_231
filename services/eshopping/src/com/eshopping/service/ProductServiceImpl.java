package com.eshopping.service;
// Generated 17 Jun, 2014 10:42:00 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eshopping.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class Product.
 * @see com.eshopping.Product
 */
@Service
public class ProductServiceImpl implements ProductService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    @Qualifier("eshopping.ProductDao")
    private WMGenericDao<Product, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Product, Integer> wmGenericDao){
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "eshoppingTransactionManager")
    @Override
    public Product create(Product product) {
        LOGGER.debug("Creating a new product with information: {}" , product);
        return this.wmGenericDao.create(product);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "eshoppingTransactionManager")
    @Override
    public Product delete(Integer productId) throws EntityNotFoundException {
        LOGGER.debug("Deleting product with id: {}" , productId);
        Product deleted = this.wmGenericDao.findById(productId);
        if (deleted == null) {
            LOGGER.debug("No product found with id: {}" , productId);
            throw new EntityNotFoundException(productId.toString());
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "eshoppingTransactionManager")
    @Override
    public Page<Product> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all products");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "eshoppingTransactionManager")
    @Override
    public Page<Product> findAll(Pageable pageable) {
        LOGGER.debug("Finding all products");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "eshoppingTransactionManager")
    @Override
    public Product findById(Integer id) throws EntityNotFoundException {
        LOGGER.debug("Finding product by id: {}" , id);
        Product product=this.wmGenericDao.findById(id);
        if(product==null){
            LOGGER.debug("No product found with id: {}" , id);
            throw new EntityNotFoundException(id.toString());
        }
        return product;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "eshoppingTransactionManager")
    @Override
    public Product update(Product updated) throws EntityNotFoundException {
        LOGGER.debug("Updating product with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((Integer)updated.getId());
    }

    @Transactional(readOnly = true, value = "eshoppingTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
    
}

