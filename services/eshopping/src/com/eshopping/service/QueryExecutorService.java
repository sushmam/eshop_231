package com.eshopping.service;
// Generated 17 Jun, 2014 10:42:00 AM 

import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.model.CustomQuery;
import com.wavemaker.runtime.data.exception.QueryParameterMismatchException;

public interface QueryExecutorService {
	
	Page<Object> executeWMCustomQuery(CustomQuery query, Pageable pageable) ;
}

