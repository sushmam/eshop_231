package com.eshopping.service;
// Generated 17 Jun, 2014 10:42:00 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.eshopping.*;
/**
 * Service object for domain model class Itemorder.
 * @see com.eshopping.Itemorder
 */

public interface ItemorderService {

   /**
	 * Creates a new itemorder.
	 * 
	 * @param created
	 *            The information of the created itemorder.
	 * @return The created itemorder.
	 */
	public Itemorder create(Itemorder created);

	/**
	 * Deletes a itemorder.
	 * 
	 * @param itemorderId
	 *            The id of the deleted itemorder.
	 * @return The deleted itemorder.
	 * @throws EntityNotFoundException
	 *             if no itemorder is found with the given id.
	 */
	public Itemorder delete(Integer itemorderId) throws EntityNotFoundException;

	/**
	 * Finds all itemorders.
	 * 
	 * @return A list of itemorders.
	 */
	public Page<Itemorder> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<Itemorder> findAll(Pageable pageable);
	
	/**
	 * Finds itemorder by id.
	 * 
	 * @param id
	 *            The id of the wanted itemorder.
	 * @return The found itemorder. If no itemorder is found, this method returns
	 *         null.
	 */
	public Itemorder findById(Integer id) throws EntityNotFoundException;

	/**
	 * Updates the information of a itemorder.
	 * 
	 * @param updated
	 *            The information of the updated itemorder.
	 * @return The updated itemorder.
	 * @throws EntityNotFoundException
	 *             if no itemorder is found with given id.
	 */
	public Itemorder update(Itemorder updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the itemorders in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the itemorder.
	 */

	public long countAll();
}

