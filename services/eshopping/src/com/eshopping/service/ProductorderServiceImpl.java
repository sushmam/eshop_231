package com.eshopping.service;
// Generated 17 Jun, 2014 10:42:00 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eshopping.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class Productorder.
 * @see com.eshopping.Productorder
 */
@Service
public class ProductorderServiceImpl implements ProductorderService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductorderServiceImpl.class);

    @Autowired
    @Qualifier("eshopping.ProductorderDao")
    private WMGenericDao<Productorder, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Productorder, Integer> wmGenericDao){
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "eshoppingTransactionManager")
    @Override
    public Productorder create(Productorder productorder) {
        LOGGER.debug("Creating a new productorder with information: {}" , productorder);
        return this.wmGenericDao.create(productorder);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "eshoppingTransactionManager")
    @Override
    public Productorder delete(Integer productorderId) throws EntityNotFoundException {
        LOGGER.debug("Deleting productorder with id: {}" , productorderId);
        Productorder deleted = this.wmGenericDao.findById(productorderId);
        if (deleted == null) {
            LOGGER.debug("No productorder found with id: {}" , productorderId);
            throw new EntityNotFoundException(productorderId.toString());
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "eshoppingTransactionManager")
    @Override
    public Page<Productorder> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all productorders");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "eshoppingTransactionManager")
    @Override
    public Page<Productorder> findAll(Pageable pageable) {
        LOGGER.debug("Finding all productorders");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "eshoppingTransactionManager")
    @Override
    public Productorder findById(Integer id) throws EntityNotFoundException {
        LOGGER.debug("Finding productorder by id: {}" , id);
        Productorder productorder=this.wmGenericDao.findById(id);
        if(productorder==null){
            LOGGER.debug("No productorder found with id: {}" , id);
            throw new EntityNotFoundException(id.toString());
        }
        return productorder;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "eshoppingTransactionManager")
    @Override
    public Productorder update(Productorder updated) throws EntityNotFoundException {
        LOGGER.debug("Updating productorder with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((Integer)updated.getId());
    }

    @Transactional(readOnly = true, value = "eshoppingTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
    
}

