package com.eshopping.service;
// Generated 17 Jun, 2014 10:42:00 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eshopping.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class Orderdetail.
 * @see com.eshopping.Orderdetail
 */
@Service
public class OrderdetailServiceImpl implements OrderdetailService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderdetailServiceImpl.class);

    @Autowired
    @Qualifier("eshopping.OrderdetailDao")
    private WMGenericDao<Orderdetail, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Orderdetail, Integer> wmGenericDao){
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "eshoppingTransactionManager")
    @Override
    public Orderdetail create(Orderdetail orderdetail) {
        LOGGER.debug("Creating a new orderdetail with information: {}" , orderdetail);
        return this.wmGenericDao.create(orderdetail);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "eshoppingTransactionManager")
    @Override
    public Orderdetail delete(Integer orderdetailId) throws EntityNotFoundException {
        LOGGER.debug("Deleting orderdetail with id: {}" , orderdetailId);
        Orderdetail deleted = this.wmGenericDao.findById(orderdetailId);
        if (deleted == null) {
            LOGGER.debug("No orderdetail found with id: {}" , orderdetailId);
            throw new EntityNotFoundException(orderdetailId.toString());
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "eshoppingTransactionManager")
    @Override
    public Page<Orderdetail> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all orderdetails");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "eshoppingTransactionManager")
    @Override
    public Page<Orderdetail> findAll(Pageable pageable) {
        LOGGER.debug("Finding all orderdetails");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "eshoppingTransactionManager")
    @Override
    public Orderdetail findById(Integer id) throws EntityNotFoundException {
        LOGGER.debug("Finding orderdetail by id: {}" , id);
        Orderdetail orderdetail=this.wmGenericDao.findById(id);
        if(orderdetail==null){
            LOGGER.debug("No orderdetail found with id: {}" , id);
            throw new EntityNotFoundException(id.toString());
        }
        return orderdetail;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "eshoppingTransactionManager")
    @Override
    public Orderdetail update(Orderdetail updated) throws EntityNotFoundException {
        LOGGER.debug("Updating orderdetail with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((Integer)updated.getId());
    }

    @Transactional(readOnly = true, value = "eshoppingTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
    
}

