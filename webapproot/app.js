Application.run(function ($rootScope, Widgets, Variables, $location, $route, DialogService) {
    "use strict";


    var projectId = $rootScope.project.id;

    /** When the user tries to access a restricted page (say user trying to access admin page)
     * we have to restrict from opening that page. */
    $rootScope.$on('$routeChangeStart', function(next, current) {
        var page = current.params.name;
        var userPages = ['Cart', 'Orders', 'Profile'],
            adminPages = ['Admin'],
            loggedInUsersPage = ['Cart', 'Orders', 'Profile', 'Admin'];

        $rootScope.userLoggedin = $rootScope.getFromLocalStorage("wm.isUserLoggedIn");
        $rootScope.isAdminLoggedIn = $rootScope.getFromLocalStorage("wm.isAdminLoggedIn");
        $rootScope.selectedItem = $rootScope.getFromLocalStorage("wm.activeProduct");

        /** If there is no activeProduct, then redirect from product detail page to main page or Admin page*/
        if(!$rootScope.selectedItem && page === "Products"){
            $rootScope.isAdminLoggedIn ? $location.path('Admin') : $location.path('Main');
        }
        /** Logged in as admin but accessing the user pages, redirect to Admin page*/
        else if($rootScope.isAdminLoggedIn && ((WM.element.inArray(page, adminPages) === -1)) ){
            $location.path('Admin');
        }
        /** Logged in as user but accessing the Admin page, redirect to Main page */
        else if($rootScope.userLoggedin && ((WM.element.inArray(page, adminPages) !== -1)) ){
            $location.path('Main');
        }
        /** Not logged in as user or admin but accessing userPages, redirect to Main page */
        else if( !$rootScope.userLoggedin &&  !$rootScope.isAdminLoggedIn && ((WM.element.inArray(page, loggedInUsersPage) !== -1)) ){
            $location.path('Main');
        }
    });


    $rootScope.addToLocalStorage = function(key, value){

        var storageObject = localStorage.getItem('eShop') ? JSON.parse(localStorage.getItem('eShop')):  {};
        if(storageObject[projectId]){
            storageObject[projectId][key] = value;
        }
        else{
            storageObject[projectId] = {};
            storageObject[projectId][key] = value;
        }
        localStorage.setItem('eShop', JSON.stringify(storageObject));
    }
    $rootScope.getFromLocalStorage = function(key){
        var value = "",
            eshopObj = localStorage.getItem('eShop');

        if(eshopObj){
            eshopObj = JSON.parse(eshopObj);
            if(eshopObj[projectId] && eshopObj[projectId][key]){
                value = eshopObj[projectId][key];
            }
        }
        return value;
    }


    $rootScope.navigateToCategory = function (category) {
        $rootScope.addToLocalStorage("wm.activeCategory", category);
    }

    $rootScope.selectedItem = {};
    $rootScope.isAdminLoggedIn = false;

    $rootScope.logout = function () {
        $rootScope.userLoggedin = false;
        $rootScope.isAdminLoggedIn = false;
        $rootScope.addToLocalStorage("wm.isUserLoggedIn", false);
        $rootScope.addToLocalStorage("wm.isAdminLoggedIn", false);
        $rootScope.addToLocalStorage("wm.currentUserObj", "");
        $location.path("Main");
    };

    $rootScope.pageLoading = false;

    /* perform any action with the variables inside this block(on-page-load) */
    $rootScope.onAppVariablesReady = function() {
        /*
         * variables can be accessed through 'Variables' service here
         * e.g. Variables.staticVariable1.getData()
         */
    };

    $rootScope.navigateToPage = function ($event, $scope) {
        $rootScope.pageLoading = true;
    }

    $rootScope.activateProductSearch = function ($event) {
        WM.element($event.currentTarget).css("width", "320px");
    }

    $rootScope.productSearchSubmit = function($event, $scope) {
        $rootScope.selectedItem = $event.data.item;
        $rootScope.selectedItem.imgUrl = $rootScope.selectedItem.imgUrl.replace("Thumbnails", "Images");
        $rootScope.addToLocalStorage("wm.activeProduct", $rootScope.selectedItem);
        if($location.$$path === "/Products") {
            $route.reload();
        } else {
            $location.path("Products");
        }
        $rootScope.pageLoading = true;
    };

    $rootScope.productSearchonBeforeUpdate = function(variable, data){
        WM.forEach(data, function(product) {
            product.imgUrl = "http://wmstudio-apps.s3.amazonaws.com/eshop/Products/Thumbnails" + product.imgUrl;
        });
    };

    $rootScope.addItemToCart = function () {
        Variables.currentProduct.dataSet = $rootScope.selectedItem;
        var item = {
            "product": Variables.currentProduct.dataSet,
            "quantity": 1,
            "status": "In Cart",
            "user": Variables.currentUser.dataSet
        };
        Variables.call("insertRow", "cartInsert", {
            "row": item
        }, function(response) {
        });

        $location.path("Cart");
    }

    $rootScope.logoutVariableonSuccess = function (variable, data) {
        $rootScope.logout();
    }

});