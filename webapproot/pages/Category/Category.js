Application.$controller("CategoryPageController", ['$rootScope', '$scope', 'Widgets', 'Variables', '$location',
    function($rootScope, $scope, Widgets, Variables, $location) {
        "use strict";

        var category;

        /** This is a callback function, that is called after a page is rendered.*/
        $scope.onPageReady = function() {
            $rootScope.pageLoading = false;
        };

        /**This is a callback function, that is called after the page variables are loaded. */
        $scope.onPageVariablesReady = function() {
            category = $rootScope.getFromLocalStorage("wm.activeCategory") || "Smartphones";
            filterVariable(category);
            $scope.productsList = Variables.productCategories.dataSet;
        }

        function filterVariable(category) {
            Variables.selectedCategory.dataSet.dataValue = category;
        }

        $scope.navigateCallback = function($event, category) {
            WM.element(".menu-link").removeClass("active");
            var currentTarget = $event.currentTarget;
            WM.element(currentTarget).find(".menu-link").addClass("active");
            if (WM.element(".app-mobile-header").css("display") !== "none") {
                WM.element(".menu-panel").toggle();
            }
            $rootScope.navigateToCategory(category);
            filterVariable(category);
        }

        /** On clicking a product, we navigate to the product detail page */
        $scope.productListClick = function($event, $scope) {
            $rootScope.selectedItem = $scope.item;
            var currentItem = $scope.item;
            currentItem.imgUrl = currentItem.imgUrl.replace("Thumbnails", "Images");
            $rootScope.addToLocalStorage("wm.activeProduct", currentItem);
            $location.path("Products");
            $rootScope.pageLoading = true;
        };

        $scope.productsonBeforeUpdate = function(variable, data) {
            WM.forEach(data, function(product) {
                product.imgUrl = "http://wmstudio-apps.s3.amazonaws.com/eshop/Products/Thumbnails" + product.imgUrl;
            });
            return data;
        };

    }
]);



Application.$controller("productListController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);