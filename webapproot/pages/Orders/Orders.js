Application.$controller("OrdersPageController", ["$scope", "$rootScope", "Widgets", "Variables",
    function($scope, $rootScope, Widgets, Variables) {
        "use strict";

        /* flag to determine if order details loaded before orders */
        $scope.orderDetailsLoaded = false;

        /* arrays storing the data from live-variables */
        $scope.ordersArray = [];
        $scope.orderDetailsArray = [];
        $scope.itemOrders = [];

        /** This is a callback function, that is called after a page is rendered.*/
        $scope.onPageReady = function() {
            $rootScope.pageLoading = false;
        };

        /**This is a callback function, that is called after the page variables are loaded. */
        $scope.onPageVariablesReady = function() {
            Variables.currentUser.dataSet = $rootScope.getFromLocalStorage("wm.currentUserObj");
        };

        /*getting orders for logged in user*/
        $scope.ordersonBeforeUpdate = function(variable, data) {
            $scope.ordersArray = data;
            /* if order-details loaded before, prepare items for live-list */
            if ($scope.orderDetailsLoaded) {
                $scope.prepareItemOrders();
            }
        };

        /*getting order details for each order for logged in user*/
        $scope.orderDetailsonBeforeUpdate = function(variable, data) {
            $scope.orderDetailsArray = data;
            /* if orders not loaded yet, set this flag */
            if (!$scope.ordersArray.length) {
                $scope.orderDetailsLoaded = true;
            } else {
                /* orders are loaded before, prepare items for live-list */
                $scope.prepareItemOrders();
            }
        };

        /* function to prepare items for live-list */
        $scope.prepareItemOrders = function() {
            Widgets.ordersList.show = $scope.ordersArray.length > 0;
            Widgets.no_orders_text.show = $scope.ordersArray.length === 0;

            WM.forEach($scope.ordersArray, function(order) {
                WM.forEach($scope.orderDetailsArray, function(orderDetails) {
                    if (order.id === orderDetails.productorder.id) {
                        $scope.itemOrders.push(orderDetails);
                    }
                });
            });
        };
    }
]);