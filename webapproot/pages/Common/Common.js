Application.$controller("CommonPageController", ['$scope', 'Widgets', 'Variables',
    function($scope, Widgets, Variables) {
        "use strict";

        /* perform any action with the variables inside this block(on-page-load) */
        $scope.onPageVariablesReady = function() {
            /*
             * variables can be accessed through 'Variables' service here
             * e.g. Variables.staticVariable1.getData()
             */
        };

    }
]);

Application.$controller("CommonLoginDialogController", ["$scope", "DialogService", "$rootScope", "$location", "Widgets", "Variables",
    function($scope, DialogService, $rootScope, $location, Widgets, Variables) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.CommonLoginDialogError = function($event, $isolateScope) {
            /*
             * Error message can be accessed from the property $isolateScope.errMsg
             */
        };

        $scope.CommonLoginDialogSuccess = function($event, $isolateScope) {
            var match = false;
            $rootScope.userLoggedin = true;
            /** Check the live variable if the credentials are right*/

            if (Widgets.usernametext.datavalue === "admin@wavemaker.com") {
                $location.path("Admin");
                $rootScope.isAdminLoggedIn = true;
                $rootScope.addToLocalStorage("wm.isAdminLoggedIn", $rootScope.isAdminLoggedIn);
            } else {
                WM.forEach(Variables.users.dataSet.data, function(userObj) {
                    if (userObj.email === Widgets.usernametext.datavalue) {

                        Variables.currentUser.dataSet = {};
                        Variables.currentUser.dataSet = userObj;
                        $rootScope.addToLocalStorage("wm.currentUserObj", userObj);

                        $rootScope.addToLocalStorage("wm.isUserLoggedIn", $rootScope.userLoggedin);
                        if ($rootScope.loginBeforeCart) {
                            $rootScope.addItemToCart();
                        }
                    }
                });
            }

            DialogService.hideDialog("CommonLoginDialog");
        };
    }
]);